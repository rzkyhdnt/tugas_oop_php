<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");

    $sheep = new Animal("shaun");
    echo "$sheep->name <br>" ;
    echo "$sheep->legs <br>" ;
    echo "$sheep->cold_blooded <br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell();

    $kodok = new Frog("buduk");
    $kodok->jump();


?>